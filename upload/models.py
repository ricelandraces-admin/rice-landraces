from django.db import models
from users.models import Organization
from django.contrib.auth.models import User
from .storage import MediaFileSystemStorage, media_file_name
import hashlib

class FieldDataFile(models.Model):
    upload_path = 'documents/'
    file_name = models.FileField(upload_to=upload_path, unique=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file_name.name

# class UploadedFile(models.Model):
#     uploaded_file = models.FileField(
#         upload_to=media_file_name, storage=MediaFileSystemStorage()
#         )
#     user = models.ForeignKey(User, on_delete=models.PROTECT)
#     uploaded_at = models.DateTimeField(auto_now_add=True)
#     md5sum = models.CharField(max_length=36, unique=True)

#     def save(self, *args, **kwargs):
#         if not self.pk:
#             md5 = hashlib.md5()
#             for chunk in self.uploaded_file.chunks():
#                 md5.update(chunk)
#             self.md5sum = md5.hexdigest()
#         super(UploadedFile, self).save(*args, **kwargs)

#     def __str__(self):
#         return self.uploaded_file.name

class FieldData(models.Model):
    file_id = models.ForeignKey(FieldDataFile, on_delete=models.CASCADE)
    site_year = models.CharField(max_length=20)
    site_name = models.CharField(max_length=20)
    trial_type = models.CharField(max_length=20)
    trial_number = models.IntegerField()
    plot_barcode = models.CharField(max_length=20)
    column_num = models.IntegerField()
    row_num = models.IntegerField()
    replicate = models.IntegerField()
    sown = models.DateField()
    genotype = models.CharField(max_length=20)
    pedigree = models.CharField(blank = True, max_length=20)
    days_to_heading = models.IntegerField(blank = True)
    days_to_maturity = models.IntegerField(blank = True)
    plant_height = models.IntegerField(blank = True)
    panicle_length = models.IntegerField(blank = True)
    panicles_per_plant = models.IntegerField(blank = True)
    tillers_per_plant = models.IntegerField(blank = True)
    grains_per_panicle = models.IntegerField(blank = True)
    yield_per_plant = models.IntegerField(blank = True)
    weight_per_thousand_grains = models.IntegerField(blank = True)
    grain_length = models.IntegerField(blank = True)
    grain_width = models.IntegerField(blank = True)
    panicle_weight = models.IntegerField(blank = True)
    panicles_per_sqm = models.IntegerField(blank = True)
    spikelets_per_panicle = models.IntegerField(blank = True)
    spikelet_fertility = models.IntegerField(blank = True)

    def __str__(self):
        return self.site_year