from django import forms
from .models import FieldDataFile
from django.forms import ValidationError
import os

class UploadFileForm(forms.ModelForm):
    class Meta:
        model = FieldDataFile
        fields = ['file_name']
        exclude = ['user']

    # def clean_field(self):
    #     data = self.cleaned_data["file_name"]
    #     f = FieldDataFile.objects.filter(file_name__endswith=data)
    #     if f.count():
    #         raise ValidationError("File already exists")
        
    #     return data
    