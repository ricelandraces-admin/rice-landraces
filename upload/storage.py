import hashlib
import os

from django.core.files.storage import FileSystemStorage
from django.db import models

class MediaFileSystemStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if max_length and len(name) > max_length:
            raise(Exception("name is greater than the max_length"))
        return name

    def _save(self, name, content):
        if self.exists(name):
            # if the file exists then do not call _save and save it
            raise(Exception("File with this name already exists!"))

        # if the file is new, call _save
        return super(MediaFileSystemStorage, self)._save(name, content)

def media_file_name(instance, filename):
    return os.path.join('uploads', filename)