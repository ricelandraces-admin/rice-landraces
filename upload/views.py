from django.shortcuts import render, redirect
from .forms import UploadFileForm
from django import forms
from .models import FieldDataFile, FieldData
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import csv
from django.conf import settings
import os
import datetime

@login_required
def model_form_upload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            upload_data = form.save(commit=False)
            print(upload_data.file_name)
            file_query_name = "/"+str(upload_data.file_name)
            if not FieldDataFile.objects.filter(file_name__endswith=file_query_name).count():
                upload_data.user = request.user
                upload_data.save()
                
                ''' upload to database (this was written to upload data on the fly as the user
                 uploads the file but commented as the format is yet to be standardized'''
                # with open(os.path.join(settings.MEDIA_ROOT, upload_data.file_name.name)) as f:
                #     reader = csv.reader(f)
                #     next(reader, None)
                #     def integer_validator(value):
                #         if type(value) == int:
                #             return value
                #         else:
                #             return None
                #     def date_formatter(date):
                #         d=datetime.datetime.strptime(date, "%d-%b-%y")
                #         return d.date().isoformat()
                #     for row in reader:
                #         _, created = FieldData.objects.get_or_create(
                #             file_id=upload_data,
                #             site_year=row[0],
                #             site_name=row[1],
                #             trial_type=row[2],
                #             trial_number=row[3],
                #             plot_barcode=row[4],
                #             column_num=row[5],
                #             row_num=row[6],
                #             replicate=row[7],
                #             sown=date_formatter(row[8]),
                #             genotype=row[9],
                #             pedigree=row[10],
                #             days_to_heading=integer_validator(row[11]),
                #             days_to_maturity=integer_validator(row[12]),
                #             plant_height=integer_validator(row[13]),
                #             panicle_length=integer_validator(row[14]),
                #             tillers_per_plant=integer_validator(row[15]),
                #             grains_per_panicle=integer_validator(row[16]),
                #             yield_per_plant=integer_validator(row[17]),
                #             weight_per_thousand_grains=integer_validator(row[18]),
                #             grain_length=integer_validator(row[19]),
                #             grain_width=integer_validator(row[20]),
                #             panicle_weight=integer_validator(row[21]),
                #             panicles_per_sqm=integer_validator(row[22]),
                #             spikelets_per_panicle=integer_validator(row[23]),
                #             spikelet_fertility=integer_validator(row[24])         
                #         ) 

                messages.success(request, f'File {upload_data.file_name} successfully uploaded.')
            else:
                messages.warning(request, f'File {upload_data.file_name} already exists.')
            return redirect('upload')
    else:
        form = UploadFileForm()
    return render(request, 'upload/upload_file.html', {'form': form})

@login_required
def list_files(request):
    current_user = request.user
    if current_user.profile.group.name == 'Site Admin':
        file_list = FieldDataFile.objects.all().order_by('-uploaded_at')
    else:
        file_list = FieldDataFile.objects.filter(user__profile__organization=current_user.profile.organization.id).order_by('-uploaded_at')
    return render(request, 'upload/file-list.html', {'file_list':file_list})