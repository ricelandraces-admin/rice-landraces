from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def collaborators(request):
    return render(request, 'collaborators.html')

def contact(request):
    return render(request, 'contact.html')
