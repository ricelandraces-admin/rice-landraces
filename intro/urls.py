from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home.html', views.index, name='index'),
    path('about.html', views.about, name='about'),
    path('collaborators.html', views.collaborators, name='collaborators'),
    path('contact.html', views.contact, name='contact'),
]