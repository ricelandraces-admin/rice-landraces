from django.contrib import admin
from users.models import Organization, Profile

#Register the Organizations Model in the admin site
admin.site.register(Organization)
admin.site.register(Profile)