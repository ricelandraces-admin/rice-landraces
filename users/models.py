from django.conf import settings
if settings.DATABASES.get('default').get('ENGINE').lower() == 'djongo':
    try:
        from djongo import models
    except ImportError:
        print("djongo not available to connect to mongo, exiting...")
        exit(1)
else:
    from django.db import models

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.db.models.signals import post_save
from django.dispatch import receiver

class Organization(models.Model):
    name = models.CharField(unique=True, max_length=150)

    def __str__(self):
        return self.name

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150, unique=True)
    organization = models.ForeignKey(Organization, default=1, on_delete=models.PROTECT)
    group = models.ForeignKey(Group, default=1 ,on_delete=models.PROTECT)
    email_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

