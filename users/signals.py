from django.contrib.auth.models import Group
from .apps import AppConfig
from .models import Organization
from django.contrib.contenttypes.models import ContentType 
def populate_models(sender, **kwargs):
    group_list = [
        "Organization User",
        "Organization Admin",
        "Site Admin"
    ]
    org_list = [
        "University of Delhi South Campus",
        "ICAR-Indian Agricultural Research Institute",
        "ICAR-National Bureau of Plant Genetic Resources",
        "ICAR-National Rice Research Institute",
        "ICAR-Indian Institute of Rice Research",
        "ICAR-Indian Agricultural Statistics Research Institute",
        "National Institute of Plant Genome Research",
        "Punjab Agriculture University",
        "Sher-e-Kashmir University of Agricultural Sciences and Technology",
        "Indira Gandhi Krishi Vishwavidyalaya",
        "Assam Agricultural University",
        "University of Agricultural Sciences (Bengaluru), ZARS",
        "Tamil Nadu Agriculture University",
        "Acharya N. G. Ranga Agricultural University, RARS"
    ]
    for group_name in group_list:
        group, created = Group.objects.get_or_create(name=group_name)
    for org_name in org_list:
        org, created = Organization.objects.get_or_create(name=org_name)