from django.contrib.auth.models import Group, User
from .models import Organization
from .models import Profile

def getApprovalAuthority(org):
    grp = Group.objects.filter(name='Organization Admin').get()
    org_admin = Profile.objects.filter(organization=org).filter(group=grp)
    if org_admin.count() > 0:
        return org_admin
    else:
        grp = Group.objects.filter(name='Site Admin').get()
        site_admin = Profile.objects.filter(group=grp)
        return site_admin
