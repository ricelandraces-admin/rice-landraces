from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import User as FormUser
from django.contrib.auth.models import User
from .models import Organization

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=150, help_text='Email')
    first_name = forms.CharField(max_length=100, help_text='First Name')
    last_name = forms.CharField(max_length=100, help_text='Last Name')
    organization = forms.ModelChoiceField(queryset=Organization.objects.all(), empty_label="**Select Organization**")

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'organization', 'password1', 'password2',)

class ResendActivationForm(forms.Form):
        registered_email = forms.EmailField(max_length=150)
