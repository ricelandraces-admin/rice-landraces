{% autoescape off %}
Hi admin,

A user with following details has requested for account activation:
username: {{ user.username }}
email: {{ user.email }}
First name: {{ user.first_name }}
Last name: {{ user.last_name }}
Organization: {{ user.profile.organization }}

Please click the following link to confirm the activation:
http://{{ domain }}{% url 'activate' uidb64=uid token=token %}

{% endautoescape %}