from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_text, force_bytes
from django.db import IntegrityError
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.mail import send_mail

from .tokens import account_activation_token
from .forms import SignUpForm, ResendActivationForm
from .authSupport import getApprovalAuthority

email_from = settings.EMAIL_HOST_USER
email_default_admin = settings.EMAIL_DEFAULT_ADMIN

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            user.profile.organization = form.cleaned_data.get('organization')
            # user won't be able to login until activated
            user.is_active = False
            user.save()

            current_site = get_current_site(request)
            username = form.cleaned_data.get('username')
            # messages.success(request, f'Account created for {username} and sent for approval.')
            subject = f'Account activation request for {username}'
            message = render_to_string('users/activation_request.txt', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            approval_authority = getApprovalAuthority(user.profile.organization) 
            # send mail to admins for approval
            for authority in approval_authority:
                authority.user.email_user(subject, message, email_from)
            # send mail to default admin (default admin is defined in django settings)
            send_mail(subject, message,email_from, email_default_admin)
            
            messages.success(request, f'Account created for {username} and sent for approval.')
            return redirect('activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'users/register.html', {'form': form})

def activation_sent(request):
    return render(request, 'users/activation_sent.html')

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        return render(request, 'users/activated.html')
    else:
        return render(request, 'users/activation_invalid.html')

def resend_activation(request):
    if request.method == 'POST':
        form = ResendActivationForm(request.POST)
        if form.is_valid():
            email=form.data.get('registered_email')
        else:
            messages.warning(request, 'Invalid data, please enter correct email.')
            return render(request, 'users/resend-activation.html', {'form': form})
        
        try:
            user=User.objects.get(email=email)
        except User.DoesNotExist:
            messages.warning(request, 'User with this email does not exist. Please enter correct email')
            return render(request, 'users/resend-activation.html', {'form': form})

        if user.is_active:
            messages.warning(request, 'Account is already activated for this user, kindly contact site admin if you are not able to login.')
            return render(request, 'users/resend-activation.html', {'form': form})  
        else:
            current_site = get_current_site(request)
            subject = f'Account activation request for {user.username}'
            message = render_to_string('users/activation_request.txt', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            approval_authority = getApprovalAuthority(user.profile.organization) 
            # send mail to admins for approval
            for authority in approval_authority:
                authority.user.email_user(subject, message, email_from)
            # send mail to default admin
            send_mail(subject, message,email_from, email_default_admin)
            
            messages.success(request, f'Request for {user.username} has been re-sent for approval.')
            return redirect('activation_sent')      
    else:
        form = ResendActivationForm()
    return render(request, 'users/resend-activation.html', {'form': form})

@login_required
def services(request):
    return render(request, 'users/services.html')

@login_required
def profile(request):
    return render(request, 'users/profile.html')