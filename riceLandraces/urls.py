"""riceLandraces URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from users import views as users_views
from upload import views as upload_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('intro/', include('intro.urls')),
    path('', include('intro.urls')),

    path('register/', users_views.register, name='register'),
    path('activation-sent/', users_views.activation_sent, name='activation_sent'),
    path('activate/<slug:uidb64>/<slug:token>/', users_views.activate, name='activate'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('services/', users_views.services, name='services'),
    path('profile/', users_views.profile, name='profile'),
    path('resend-activation-link/', users_views.resend_activation, name='resend_activation'),
    path('password-change/', auth_views.PasswordChangeView.as_view(template_name='users/password-change.html'), name='password_change'),
    path('password-change/done/', auth_views.PasswordChangeDoneView.as_view(template_name='users/password-change-done.html'), name='password_change_done'),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password-reset.html'), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/password-reset-done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/password-reset-confirm.html'), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='users/password-reset-complete.html'), name='password_reset_complete'),

    path('upload/', upload_views.model_form_upload, name = 'upload'),
    path('upload/uploaded-files/', upload_views.list_files, name='uploaded-files'),
]
